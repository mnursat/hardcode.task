## Постановка задачи:
Создать ASP.NET Core Web API (net core 3.1+) приложение, в котором предполагается следующий функционал:
- Категории товара
- Создание товара
- Просмотр списка товаров
- Просмотр товара

1. Страница категории товара.
На странице категорий товара должен выводится список категорий, с возможностью добавлять новую или удалить существующую категорию.
При добавлении категории нужно добавить возможность добавлять дополнительные поля для товара этой категории, например, цвет, вес, размер и т.д.

2. Страница создание товара
На странице создание товара должна быть возможность заполнить общую информацию о товаре (фотография, название, описание, цена, категория) и указать значения для дополнительных полей, которые были созданы для выбранной категории.

3. Страница просмотра списка товара
Вывести список товаров с фильтром по категориям и дополнительным полям

4. Просмотр выбранного товара из списка товаров

Обязательная часть включает в себя создание всего двух эндпоинтов, реализующих сохранение и получение товаров. Основой целью является проверка подхода к проектированию механизма "динамических полей", которые создаются в категории, и должны заполняться и возвращаться в товаре.

В рамках выполнения задания необходимо подготовить только API для данного функционала. Обязательными для реализации являются API получение товаров с фильтрацией и добавление товаров, остальные на ваше усмотрение. 

Результат выложить в gitlab, и предоставить ссылку на репозиторий для оценки реализации. В репозитории должна быть основная ветка (например main), а каждая подзадача должна выполняться в отдельной ветке, по выполнению которой ветка подзадачи должна быть слита в основную путем создания merge request'а.

# HardCode.Task API

- [HardCode.Review API](#hardcode-api)
  - [Create Product](#create-product)
    - [Create Product Request](#create-product-request)
    - [Create Product Response](#create-product-response)
  - [Get Product](#get-product)
    - [Get Product Request](#get-product-request)
    - [Get Product Response](#get-product-response)
  - [Update Product](#update-product)
    - [Update Product Request](#update-product-request)
    - [Update Product Response](#update-product-response)
  - [Delete Product](#delte-product)
    - [Delete Product Request](#delete-product-request)
    - [Delete Product Response](#delete-product-response)

## Usage

To create local db

```
  update-database
```