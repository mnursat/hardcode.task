﻿using HardCode.Task.Domain.Entities;

namespace HardCode.Task.Contracts.Product;

public record ProductResponse(
    Guid id,
     string Title,
    string Description,
    int Price,
    string ImageURL,
    HardCode.Task.Domain.Entities.Category Category,
    List<AddedProperty> Properties);
