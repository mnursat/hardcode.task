﻿namespace HardCode.Task.Contracts.Category;

public record CreateCategoryRequest(
    string Title);
