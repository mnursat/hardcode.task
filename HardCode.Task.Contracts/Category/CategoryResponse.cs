﻿using HardCode.Task.Domain.Entities;

namespace HardCode.Task.Contracts.Category;

public record CategoryResponse(
    Guid id,
    string Title,
    List<HardCode.Task.Domain.Entities.Product> Products);
