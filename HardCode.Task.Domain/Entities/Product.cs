﻿namespace HardCode.Task.Domain.Entities;

public class Product
{
    public Guid Id { get; set; }
    public string Title { get; set; }
    public string Description { get; set; }
    public int Price { get; set; }
    public string ImageURL { get; set; }
    public Category Category { get; set; }
    public List<AddedProperty> Properties { get; set; }
}
