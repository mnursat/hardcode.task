﻿namespace HardCode.Task.Domain.Entities;

public class AddedProperty
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public string Value { get; set; }
}
