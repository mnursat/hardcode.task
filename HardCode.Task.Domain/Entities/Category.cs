﻿namespace HardCode.Task.Domain.Entities;

public class Category
{
    public Guid Id { get; set; }
    public string Title { get; set; }
    public List<Product> Products { get; set; } = new();
}
