﻿using HardCode.Task.Domain.Entities;

namespace HardCode.Task.Api.Services.Data;

public class CategoryService : ICategoryService
{
    private readonly ApplicationDbContext _db;

    public CategoryService(ApplicationDbContext db)
    {
        _db = db;
    }

    public void CreateCategory(Category category)
    {
        _db.Categories.Add(category);
        _db.SaveChanges();
    }

    public void DeleteCategory(Guid id)
    {
        var category = _db.Categories.FirstOrDefault(c => c.Id == id);

        if (category is null)
        {
            throw new Exception("Category id was not found");
        }

        _db.Categories.Remove(category);
        _db.SaveChanges();
    }

    public ICollection<Category> GetAllCategories()
    {
        return _db.Categories.ToList();
    }
}
