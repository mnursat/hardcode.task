﻿using HardCode.Task.Domain.Entities;

namespace HardCode.Task.Api.Services.Data;

public interface ICategoryService
{
    void CreateCategory(Category category);
    void DeleteCategory(Guid id);
    ICollection<Category> GetAllCategories();
}
