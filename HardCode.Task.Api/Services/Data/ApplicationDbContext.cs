﻿using HardCode.Task.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace HardCode.Task.Api.Services.Data;

public class ApplicationDbContext : DbContext
{
    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> opt)
        : base(opt)
    {
        
    }

    public DbSet<Product> Products { get; set; }
    public DbSet<Category> Categories { get; set; }
}
