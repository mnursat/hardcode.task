﻿using HardCode.Task.Domain.Entities;

namespace HardCode.Task.Api.Services.Data;

public interface IProductService
{
    void CreateProduct(Product product);
    Product GetProduct(Guid id);
    ICollection<Product> GetAllProducts();
    ICollection<Product> GetAllProductsFilteredByName();
    ICollection<Product> GetAllProductsFilteredByPrice();
}
