﻿using HardCode.Task.Domain.Entities;

namespace HardCode.Task.Api.Services.Data;

public class ProductService : IProductService
{
    private readonly ApplicationDbContext _db;

    public ProductService(ApplicationDbContext db)
    {
        _db = db;
    }

    public void CreateProduct(Product product)
    {
        _db.Products.Add(product);
        _db.SaveChanges();
    }

    public ICollection<Product> GetAllProducts()
    {
        return _db.Products.ToList();
    }

    public Product GetProduct(Guid id)
    {
        var product = _db.Products.FirstOrDefault(p => p.Id == id);

        if (product is null)
        {
            return null;
        }

        return product;
    }

    public ICollection<Product> GetAllProductsFilteredByName()
    {
        return _db.Products.OrderBy(p => p.Title).ToList();
    }

    public ICollection<Product> GetAllProductsFilteredByPrice()
    {
        return _db.Products.OrderBy(p =>  p.Price).ToList();
    }
}
