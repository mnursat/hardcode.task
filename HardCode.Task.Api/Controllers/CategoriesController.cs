﻿using HardCode.Task.Api.Services.Data;
using HardCode.Task.Contracts.Category;
using HardCode.Task.Domain.Entities;
using Microsoft.AspNetCore.Mvc;

namespace HardCode.Task.Api.Controllers;

[Route("api/[controller]")]
[ApiController]
public class CategoriesController : ControllerBase
{
    private readonly ICategoryService _categoryService;

    public CategoriesController(ICategoryService categoryService)
    {
        _categoryService = categoryService;
    }

    [HttpPost]
    public IActionResult CreateCategory(CreateCategoryRequest request)
    {
        var category = new Category()
        {
            Id = Guid.NewGuid(),
            Title = request.Title,
        };

        _categoryService.CreateCategory(category);

        var response = new CategoryResponse(
            category.Id,
            category.Title,
            category.Products);

        return CreatedAtAction(
            actionName: nameof(GetAllCategory),
            routeValues: new { id = category.Id },
            value: response);
    }

    [HttpGet]
    public IActionResult GetAllCategory()
    {
        return Ok(_categoryService.GetAllCategories());
    }

    [HttpDelete("{id:guid}")]
    public IActionResult DeleteCategory(Guid id)
    {
        _categoryService.DeleteCategory(id);
        return NoContent();
    }
}
