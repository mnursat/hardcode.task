﻿using HardCode.Task.Api.Services.Data;
using HardCode.Task.Contracts.Product;
using HardCode.Task.Domain.Entities;
using Microsoft.AspNetCore.Mvc;

namespace HardCode.Task.Api.Controllers;

[Route("api/[controller]")]
[ApiController]
public class ProductsController : ControllerBase
{
    private readonly IProductService _productService;

    public ProductsController(IProductService productService)
    {
        _productService = productService;
    }

    [HttpPost]
    public IActionResult CreateProduct(CreateProductRequest request)
    {
        var product = new Product
        {
            Id = Guid.NewGuid(),
            Title = request.Title,
            Description = request.Description,
            Price = request.Price,
            ImageURL = request.ImageURL,
            Category = request.Category,
            Properties = request.Properties,
        };

        var response = new ProductResponse(
            product.Id,
            product.Title,
            product.Description,
            product.Price,
            product.ImageURL,
            product.Category,
            product.Properties);

        return CreatedAtAction(
            actionName: nameof(GetProduct),
            routeValues: new { id = product.Id },
            value: response);
    }

    public IActionResult GetProduct(Guid id)
    {
        var product = _productService.GetProduct(id);

        if (product is null) return NotFound($"Product with {id} not found");

        return Ok(product);
    }
  
}
