using HardCode.Task.Api.Services.Data;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);
{
    builder.Services.AddControllers();

    string connectionString = builder.Configuration.GetConnectionString("local");
    builder.Services.AddDbContext<ApplicationDbContext>(options =>
    {
        options.UseSqlite($"Data Source={connectionString}");
    });

    builder.Services.AddScoped<ICategoryService, CategoryService>();
}

var app = builder.Build();
{
    app.UseHttpsRedirection();
    app.MapControllers();
    app.Run();
}